## Entity Limit Extend
Extend the capabilities of the Entity Limit module with enhanced features for advanced access control. 
This module is designed as an extension to the Entity Limit module, providing additional granularity 
in limit configurations.

## Key Features
Daily Limits: Set specific access limits for users on a daily basis.
Weekly Limits: Define access restrictions for users on a weekly cycle.
Monthly Limits: Configure limits based on a monthly cycle for fine-grained control.
Yearly Limits: Implement access limits on a yearly basis for long-term management.

## Requirements
 - Drupal 9 or 10.
 - Entity Limit module (Ensure it is installed and enabled).

## Installation
 - Download the Entity Limit Extend module.
 - Extract the module to the modules directory of your Drupal installation.
 - Enable the module through the Drupal admin interface or using Drush.

## Troubleshooting:
 - Verify that the Entity Limit module is installed and enabled.
 - Check the configuration settings for conflicting limits.