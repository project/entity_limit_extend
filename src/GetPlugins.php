<?php

namespace Drupal\entity_limit_extend;

use Drupal\entity_limit\Plugin\EntityLimit\UserLimit;

/**
 * Custom class that combines RoleLimit and UserLimit functionality.
 */
class GetPlugins extends UserLimit {

  /**
   * Get user id from userlimit plugin.
   */
  public static function getUserLimitPlugin() {
    $userLimitInstance = UserLimit::create(\Drupal::getContainer(), [], 'user_limit', []);
    $uid = $userLimitInstance->account->id();
    return $uid;
  }

}
