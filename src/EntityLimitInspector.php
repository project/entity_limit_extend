<?php

namespace Drupal\entity_limit_extend;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_limit\Entity\EntityLimit;
use Drupal\entity_limit\Plugin\EntityLimit\RoleLimit;

/**
 * Provide handler for all entity limit usage functions.
 */
class EntityLimitInspector extends RoleLimit {

  /**
   * Unilimited limit option value.
   *
   * @var int
   */
  const ENTITYLIMITNOLIMIT = -1;

  /**
   * {@inheritdoc}
   */
  protected $entityManager;

  /**
   * {@inheritdoc}
   */
  protected $pluginManager;

  /**
   * Construct entity_limit usage.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   Entity Manager.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $pluginManager
   *   Plugin Manager.
   */
  public function __construct(EntityTypeManagerInterface $entityManager, PluginManagerInterface $pluginManager) {
    $this->entityManager = $entityManager;
    $this->pluginManager = $pluginManager;
  }

  /**
   * Gets the entity limits.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   *
   * @return array
   *   The entity limits.
   */
  public function getEntityLimits($entity_type_id) {
    $configStorage = $this->entityManager->getStorage('entity_limit');
    $configurations = $configStorage->loadByProperties([
      'entity_type' => $entity_type_id,
    ]
    );
    return $configurations;
  }

  /**
   * Check whether user has crossed the entity limits.
   *
   * @param string $entity_type_id
   *   Entity type.
   * @param string $entity_bundle
   *   Entity bundle.
   *
   * @return array|mixed
   *   Array with limit_count and entity_limit object.
   */
  public function checkEntityLimitAccess($entity_type_id, $entity_bundle) {
    $entity_limits = $this->getBundleLimits($entity_type_id, $entity_bundle);
    $applicable_limits = [];
    $plugin = [];

    if (!empty($entity_limits)) {
      foreach ($entity_limits as $entity_limit) {
        $plugin_id = $entity_limit->getPlugin();
        if (!isset($plugin[$plugin_id])) {
          $plugin[$plugin_id] = $this->pluginManager->createInstance($plugin_id, ['of' => 'configuration values']);
        }
        $plugin_priority = $plugin[$plugin_id]->getPriority();
        $weight = $entity_limit->get('weight');
        $limit_count = $plugin[$plugin_id]->getLimitCount($entity_limit);
        if (isset($applicable_limits[$weight]) && is_array($applicable_limits[$weight])) {
          $applicable_limits[$weight][$plugin_priority][$limit_count] = $entity_limit;
        }
        else {
          $applicable_limits[$weight] = [];
          $applicable_limits[$weight][$plugin_priority] = [];
          $applicable_limits[$weight][$plugin_priority][$limit_count] = $entity_limit;
        }
      }

      // Sort in the order of entity limit priority.
      ksort($applicable_limits);
      // There can be two cases
      // 1. Multiple applicable limits of different priority. In this case
      // we will give access to the top priority item.
      // 2. Multiple applicable limits of same priority. In this case we will
      // goto plugin priority.
      // 2.1. If there are multiple limits of same plugin priority & entity
      // limit priority then we consider the height limit value from the set.
      $applicable_limits = reset($applicable_limits);
      ksort($applicable_limits);
      $applicable_limits = reset($applicable_limits);
    }
    $access = !empty($applicable_limits) ? $this->compareLimits($applicable_limits, $plugin) : TRUE;
    return $access;
  }

  /**
   * Compare applicable limit with availability.
   *
   * @param array $applicable_limits
   *   Array of Entity Limit objects.
   * @param array $plugin
   *   Array of Plugin objects.
   *
   * @return bool
   *   Access.
   */
  public function compareLimits(array $applicable_limits, array $plugin) {
    $access = TRUE;
    if (!empty($applicable_limits)) {
      if (array_key_exists(self::ENTITYLIMITNOLIMIT, $applicable_limits)) {
        $access = TRUE;
      }
      else {
        $max_limit = max(array_keys($applicable_limits));
        $plugin_type = $applicable_limits[$max_limit]->getPlugin();
        if ($plugin_type == 'role_limit') {
          $access = $this->checkRoleAccess($max_limit, $applicable_limits[$max_limit]);
        }
        if ($plugin_type == 'user_limit') {
          $access = $this->checkUserAccess($max_limit, $applicable_limits[$max_limit]);
        }
      }
    }
    return $access;
  }

  /**
   * Gets the bundle limits.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   * @param string $entity_bundle
   *   The entity bundle.
   *
   * @return array
   *   The bundle limits.
   */
  public function getBundleLimits($entity_type_id, $entity_bundle) {
    $entity_limits = $this->getEntityLimits($entity_type_id);
    $applicable_limits = [];
    foreach ($entity_limits as $key => $entity_limit) {
      if (in_array($entity_bundle, $entity_limit->getEntityLimitBundles())) {
        $applicable_limits[$key] = $entity_limit;
      }
    }
    return $applicable_limits;
  }

  /**
   * Compare limits and provide access.
   *
   * @param int $limit
   *   The limit.
   * @param \Drupal\entity_limit\Entity\EntityLimit $entityLimit
   *   The entity limit.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   Optional User account.
   *
   * @return bool
   *   TRUE|FALSE for access.
   */
  public function checkRoleAccess($limit, EntityLimit $entityLimit, AccountInterface $account = NULL) {
    if ($account) {
      $uid = $account->id();
      $account_roles = $account->getRoles();
    }
    else {
      $roleLimitInstance = RoleLimit::create(\Drupal::getContainer(), [], 'role_limit', []);
      $uid = $roleLimitInstance->account->id();
      $account_roles = $roleLimitInstance->account->getRoles();
    }

    $fields = \Drupal::service('entity_field.manager')
      ->getFieldStorageDefinitions($entityLimit->getEntityLimitType());
    $uid_field = !empty($fields['user_id']) ? 'user_id' : 'uid';

    $access = TRUE;
    $query = \Drupal::entityQuery($entityLimit->getEntityLimitType());
    $query->condition('type', $entityLimit->getEntityLimitBundles(), 'IN');
    $query->condition($uid_field, $uid)->accessCheck(TRUE);
    $get_limit_type = $this->getRoleLimitPeriod($entityLimit->get('limits'), $account_roles);
    if ($get_limit_type == 'daily') {
      $current_date = new DrupalDateTime('now');
      $current_date->setTimezone(new \DateTimezone('UTC'));
      $start_of_day = strtotime($current_date->format('Y-m-d 00:00:00'));
      $end_of_day = strtotime($current_date->format('Y-m-d 23:59:59'));
      $query->condition('created', [$start_of_day, $end_of_day], 'BETWEEN');
    }
    elseif ($get_limit_type == 'weekly') {
      $current_utc_time = \Drupal::time()->getCurrentTime();
      $start_of_week = strtotime('last sunday', $current_utc_time);
      $end_of_week = strtotime('next sunday', $start_of_week);
      $query->condition('created', [$start_of_week, $end_of_week], 'BETWEEN');

    }
    elseif ($get_limit_type == 'monthly') {
      $current_month = date('m');
      $current_year = date('Y');
      $current_date = strtotime("now");
      $month_start_timestmp = strtotime("$current_month/01/$current_year");
      $query->condition('created', $month_start_timestmp, '>=');
      $query->condition('created', $current_date, '<=');
    }
    elseif ($get_limit_type == 'yearly') {
      $current_utc_time = \Drupal::time()->getCurrentTime();
      $year = date('Y');
      $year_start_utc = strtotime("January 1, $year 00:00:00", $current_utc_time);
      $query->condition('created', [$year_start_utc, $current_utc_time], 'BETWEEN');
    }
    $count = count($query->execute());
    if ($count >= (int) $limit) {
      $access = FALSE;
    }
    return $access;
  }

  /**
   * Get limit type from applicable role if user has multiple roles.
   */
  public function getRoleLimitPeriod($entityLimit, $account_roles = NULL) {
    $limit_type = $entity_limit = [];
    $highest_limit = 0;
    foreach ($entityLimit as $limit) {
      $limit_type[$limit['id']] = $limit['limit_type'];
      $entity_limit[$limit['id']] = $limit['limit'];
    }
    // If multiple roles, then take highest limit and monthly check from them.
    foreach ($account_roles as $role) {
      $temp = (isset($entity_limit[$role])) ? $entity_limit[$role] : NULL;
      if ($temp > $highest_limit) {
        $highest_limit = $temp;
        $highest_limit_type = $limit_type[$role];
      }
    }
    return $highest_limit_type;
  }

  /**
   * Compare limits and provide access.
   *
   * @param int $limit
   *   The limit.
   * @param \Drupal\entity_limit\Entity\EntityLimit $entityLimit
   *   The entity limit.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   Optional User account.
   *
   * @return bool
   *   TRUE|FALSE for access.
   */
  public function checkUserAccess($limit, EntityLimit $entityLimit, AccountInterface $account = NULL) {
    if ($account) {
      $uid = $account->id();
    }
    else {
      $uid = GetPlugins::getUserLimitPlugin();
    }
    $fields = \Drupal::service('entity_field.manager')
      ->getFieldStorageDefinitions($entityLimit->getEntityLimitType());
    $uid_field = !empty($fields['user_id']) ? 'user_id' : 'uid';

    $access = TRUE;
    $query = \Drupal::entityQuery($entityLimit->getEntityLimitType());
    $query->condition('type', $entityLimit->getEntityLimitBundles(), 'IN');
    $query->condition($uid_field, $uid)->accessCheck(TRUE);

    $get_limit_type = $this->getUserLimitPeriod($entityLimit->get('limits'), $uid);
    if ($get_limit_type == 'daily') {
      $current_date = new DrupalDateTime('now');
      $current_date->setTimezone(new \DateTimezone('UTC'));
      $start_of_day = strtotime($current_date->format('Y-m-d 00:00:00'));
      $end_of_day = strtotime($current_date->format('Y-m-d 23:59:59'));
      $query->condition('created', [$start_of_day, $end_of_day], 'BETWEEN');
    }
    elseif ($get_limit_type == 'weekly') {
      $current_utc_time = \Drupal::time()->getCurrentTime();
      $start_of_week = strtotime('last sunday', $current_utc_time);
      $end_of_week = strtotime('next sunday', $start_of_week);
      $query->condition('created', [$start_of_week, $end_of_week], 'BETWEEN');

    }
    elseif ($get_limit_type == 'monthly') {
      $current_month = date('m');
      $current_year = date('Y');
      $current_date = strtotime("now");
      $month_start_timestmp = strtotime("$current_month/01/$current_year");
      $query->condition('created', $month_start_timestmp, '>=');
      $query->condition('created', $current_date, '<=');
    }
    elseif ($get_limit_type == 'yearly') {
      $current_utc_time = \Drupal::time()->getCurrentTime();
      $year = date('Y');
      $year_start_utc = strtotime("January 1, $year 00:00:00", $current_utc_time);
      $query->condition('created', [$year_start_utc, $current_utc_time], 'BETWEEN');
    }
    $count = count($query->execute());
    if ($count >= (int) $limit) {
      $access = FALSE;
    }
    return $access;
  }

  /**
   * Get limit type specific user.
   */
  public function getUserLimitPeriod($entityLimit, $uid = NULL) {
    if ($uid) {
      foreach ($entityLimit as $limit) {
        $get_limit_type[$limit['id']] = $limit['limit_type'];
      }
      return $get_limit_type[$uid] ?? 0;
    }
  }

}
